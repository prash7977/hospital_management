<?php

	//session_start();
	//print_r($_SESSION);
	//echo $_SESSION["username"];
	//echo $_SESSION["favcolor"];
	//echo isset($_SESSION["username"]);

	//echo "<script>alert( 'Debug Objects: " . $_SESSION["username"] . "' );</script>";

	if(!isset($_SESSION["username"])){
		header("Location: login.php");
		exit(); 
	}

	function debug_to_console( $data ) {
		$output = $data;
		if ( is_array( $output ) )
		    $output = implode( ',', $output);

		echo "<script>alert( 'Debug Objects: " . $output . "' );</script>";
	}
?>